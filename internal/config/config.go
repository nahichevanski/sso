package config

import (
	"github.com/ilyakaznacheev/cleanenv"
	"github.com/joho/godotenv"
	"time"
)

type Config struct {
	Env             string        `yaml:"env" env-required:"true"`
	DB              db            `yaml:"db" env-required:"true"`
	Redis           redis         `yaml:"redis" env-required:"true"`
	GRPC            gRPC          `yaml:"grpc" env-required:"true"`
	AccessTokenTTL  time.Duration `yaml:"access_token_ttl" env-required:"true"`
	RefreshTokenTTL time.Duration `yaml:"refresh_token_ttl" env-required:"true"`
	SessionTTL      time.Duration `yaml:"session_ttl" env-required:"true"`
}

type gRPC struct {
	Port    int           `yaml:"port" env-required:"true"`
	Timeout time.Duration `yaml:"timeout" env-required:"true"`
}

type db struct {
	Host              string        `yaml:"host" env-required:"true"`
	Port              string        `yaml:"port" env-required:"true"`
	User              string        `yaml:"user" env-required:"true"`
	Name              string        `yaml:"name" env-required:"true"`
	ConnectionTimeout time.Duration `yaml:"connection_timeout" env-required:"true"`
	QueryTimeout      time.Duration `yaml:"query_timeout" env-required:"true"`
	Password          string        `env:"POSTGRES_PASSWORD"`
}

type redis struct {
	Addr     string `yaml:"addr" env-required:"true"`
	Password string `env:"REDIS_PASSWORD"`
}

var cfg Config

func MustLoad() *Config {

	err := cleanenv.ReadConfig("./config/prod.yaml", &cfg)
	if err != nil {
		panic(err)
	}

	err = godotenv.Load(".env")
	if err != nil {
		panic(err)
	}

	return &cfg
}
