package serverauth

import (
	"context"
	"errors"
	proto "gitlab.com/nahichevanski/proto-sso/gen/go/sso"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"sso/internal/storage"
)

func (s *server) IsAdmin(ctx context.Context, req *proto.IsAdminRequest) (*proto.IsAdminResponse, error) {
	userID := req.GetUserId()
	if userID == 0 {
		return nil, status.Error(codes.InvalidArgument, "missing user_id")
	}

	isAdmin, err := s.auth.IsAdmin(ctx, userID)
	if err != nil {
		if errors.Is(err, storage.ErrUserNotFound) {
			return nil, status.Error(codes.NotFound, "user not found")
		}
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &proto.IsAdminResponse{IsAdmin: isAdmin}, nil
}
