package serverauth

import (
	"context"
	proto "gitlab.com/nahichevanski/proto-sso/gen/go/sso"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *server) Delete(ctx context.Context, req *proto.DeleteRequest) (*proto.DeleteResponse, error) {
	id := req.GetUserId()
	if id == 0 {
		return nil, status.Error(codes.InvalidArgument, "user id don't be equal 0, but equal 0")
	}

	wasDelete, err := s.auth.RemoveUser(ctx, id)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &proto.DeleteResponse{WasDelete: wasDelete}, nil
}
