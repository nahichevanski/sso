package serverauth

import (
	"context"
	proto "gitlab.com/nahichevanski/proto-sso/gen/go/sso"
	"google.golang.org/grpc"
	"sso/internal/domain/model"
)

type Auth interface {
	RegisterNewUser(ctx context.Context, email, password string) (int64, error)
	EnterUser(ctx context.Context, email, password string) (model.Token, error)
	IsAdmin(ctx context.Context, userID int64) (bool, error)
	OutUser(ctx context.Context, userID int64) (bool, error)
	RemoveUser(ctx context.Context, userID int64) (bool, error)
	RefreshToken(ctx context.Context, userID int64, refreshToken string) (model.Token, error)
}

type server struct {
	proto.UnimplementedAuthServer
	auth Auth
}

func Register(gRPC *grpc.Server, auth Auth) {
	proto.RegisterAuthServer(gRPC, &server{auth: auth})
}
