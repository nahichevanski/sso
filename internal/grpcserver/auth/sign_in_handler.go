package serverauth

import (
	"context"
	"errors"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	proto "gitlab.com/nahichevanski/proto-sso/gen/go/sso"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"sso/internal/service/auth"
)

func (s *server) SignIn(ctx context.Context, req *proto.SignInRequest) (*proto.SignInResponse, error) {
	email := req.GetEmail()
	err := validation.Validate(email, validation.Required, is.Email)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	password := req.GetPassword()
	err = validation.Validate(
		password, validation.Required, validation.Length(8, 20), is.Alphanumeric,
	)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	token, err := s.auth.EnterUser(ctx, email, password)
	if err != nil {
		if errors.Is(err, serviceauth.ErrInvalidCredentials) {
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &proto.SignInResponse{
		AccessToken:  token.AccessToken,
		RefreshToken: token.RefreshToken,
	}, nil
}
