package serverauth

import (
	"context"
	"errors"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	proto "gitlab.com/nahichevanski/proto-sso/gen/go/sso"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	serviceauth "sso/internal/service/auth"
)

func (s *server) SignUp(ctx context.Context, req *proto.SignUpRequest) (*proto.SignUpResponse, error) {
	email := req.GetEmail()
	err := validation.Validate(email, validation.Required, is.Email)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	password := req.GetPassword()
	err = validation.Validate(
		password, validation.Required, validation.Length(8, 20), is.Alphanumeric,
	)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	userID, err := s.auth.RegisterNewUser(ctx, email, password)
	if err != nil {
		if errors.Is(err, serviceauth.ErrUserExists) {
			return nil, status.Error(codes.AlreadyExists, err.Error())
		}
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &proto.SignUpResponse{
		UserId: userID,
	}, nil
}
