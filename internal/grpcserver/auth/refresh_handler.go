package serverauth

import (
	"context"
	"errors"
	proto "gitlab.com/nahichevanski/proto-sso/gen/go/sso"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	serviceauth "sso/internal/service/auth"
)

func (s *server) Refresh(ctx context.Context, req *proto.RefreshRequest) (*proto.RefreshResponse, error) {
	aToken := req.GetAccessToken()
	if aToken == "" {
		return nil, status.Error(codes.InvalidArgument, "missing access token")
	}

	rToken := req.GetRefreshToken()
	if rToken == "" {
		return nil, status.Error(codes.InvalidArgument, "missing refresh token")
	}

	userId := req.GetUserId()

	token, err := s.auth.RefreshToken(ctx, userId, rToken)
	if err != nil {
		if errors.Is(err, serviceauth.ErrTokenExpired) {
			return nil, status.Error(codes.Unauthenticated, "session expired")
		}
		if errors.Is(err, serviceauth.ErrTokenNotMatch) {
			return nil, status.Error(codes.Unauthenticated, "token not match")
		}
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &proto.RefreshResponse{
		AccessToken:  token.AccessToken,
		RefreshToken: token.RefreshToken,
	}, nil
}
