package serverauth

import (
	"context"
	proto "gitlab.com/nahichevanski/proto-sso/gen/go/sso"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *server) SignOut(ctx context.Context, req *proto.SignOutRequest) (*proto.SignOutResponse, error) {
	isOut, err := s.auth.OutUser(ctx, req.GetUserId())
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &proto.SignOutResponse{
		IsOut: isOut,
	}, nil
}
