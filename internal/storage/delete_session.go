package storage

import (
	"context"
	"errors"
	"fmt"
)

func (s *Storage) DeleteSession(ctx context.Context, userId int64) error {
	const oper = "storage.DeleteSession"

	cmd := s.cache.Del(ctx, fmt.Sprintf("%d", userId))
	if err := cmd.Err(); err != nil {
		return fmt.Errorf("%s: %w", oper, err)
	}
	if cmd.Val() == 0 {
		return errors.New("session not found")
	}

	return nil
}
