package storage

import (
	"context"
	"fmt"
	"sso/internal/domain/model"
	"strconv"
	"time"
)

func (s *Storage) RefreshSession(ctx context.Context, newSession model.Session, oldRefreshToken string) error {
	const oper = "storage.RefreshSession"

	userID := fmt.Sprintf("%d", newSession.UserID)

	token := s.cache.HGet(ctx, userID, refreshToken)
	if token.Err() != nil {
		return fmt.Errorf("%s: %w", oper, token.Err())
	}
	if token.Val() != oldRefreshToken {
		return fmt.Errorf("%s: %w", oper, ErrTokenNotMatch)
	}

	expStr := s.cache.HGet(ctx, userID, expiredAt)
	if token.Err() != nil {
		return fmt.Errorf("%s: %w", oper, expStr.Err())
	}

	exp, err := expStr.Time()
	if err != nil {
		return fmt.Errorf("%s: %w", oper, expStr.Err())
	}
	if exp.Before(time.Now()) {
		return fmt.Errorf("%s: %w", oper, ErrTokenExpired)
	}

	cmd := s.cache.HSet(ctx, userID,
		refreshToken, newSession.RefreshToken, expiredAt, newSession.ExpiresAt,
	)
	if cmd.Err() != nil {
		return fmt.Errorf("%s: %w", oper, cmd.Err())
	}

	boolCmd := s.cache.Expire(ctx, strconv.FormatInt(newSession.UserID, 10), newSession.Lifetime)
	if err = boolCmd.Err(); err != nil {
		return fmt.Errorf("%s: %w", oper, err)
	}

	return nil
}
