package postgres

import (
	"context"
	"fmt"
	"log/slog"
	"os"
	"time"

	"github.com/jackc/pgx/v5/pgxpool"

	"sso/internal/config"
)

func New(log *slog.Logger, cfg *config.Config) (*pgxpool.Pool, error) {
	const oper = "storage.postgres.New"

	ctx := context.Background()

	conn := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
		cfg.DB.User, os.Getenv("POSTGRES_PASSWORD"), cfg.DB.Host, cfg.DB.Port, cfg.DB.Name)

	connectConfig, err := pgxpool.ParseConfig(conn)
	if err != nil {
		log.Error("Failed to parse connection string")
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	connectConfig.ConnConfig.ConnectTimeout = cfg.DB.ConnectionTimeout

	var db *pgxpool.Pool

	for i := 0; i < 10; i++ {
		db, err = pgxpool.NewWithConfig(ctx, connectConfig)
		if err != nil {
			log.Error(fmt.Sprintf("Failed connection - %d attempts left", 9-i))
			time.Sleep(time.Duration(i) * 100 * time.Millisecond)
		} else {
			log.Info("Postgres connection successful", slog.Int("attempts", i+1))
			break
		}
	}

	if err != nil {
		log.Error("Postgres connection failed")
		return nil, fmt.Errorf("%s: %w", oper, err)
	}
	return db, nil
}
