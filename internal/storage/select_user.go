package storage

import (
	"context"
	"errors"
	"fmt"

	"github.com/jackc/pgx/v5"

	"sso/internal/domain/model"
)

func (s *Storage) SelectUser(ctx context.Context, email string) (model.User, error) {
	const oper = "postgres.SelectUser"

	user := model.User{}

	query := `SELECT * FROM users WHERE email = $1`

	err := s.db.QueryRow(ctx, query, email).Scan(&user.ID, &user.Email, &user.PassHash)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return model.User{}, ErrUserNotFound
		}
		return model.User{}, fmt.Errorf("%s: %w", oper, err)
	}

	return user, nil
}
