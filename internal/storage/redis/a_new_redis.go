package redis

import (
	"context"
	"fmt"
	"github.com/redis/go-redis/v9"
	"log/slog"
	"sso/internal/config"
	"sso/internal/lib/sl"
)

func New(log *slog.Logger, cfg *config.Config) (*redis.Client, error) {
	const oper = "redis.New"

	rds := redis.NewClient(
		&redis.Options{
			Addr:     cfg.Redis.Addr,
			Password: cfg.Redis.Password,
			DB:       0,
		},
	)

	status, err := rds.Ping(context.Background()).Result()
	if err != nil {
		log.Error(oper, sl.Err(err))
		return nil, fmt.Errorf("%s: %w", oper, err)
	}
	log.Info("redis connection successful", slog.String("status", status))

	return rds, nil
}
