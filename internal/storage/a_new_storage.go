package storage

import (
	"errors"
	"github.com/jackc/pgx/v5/pgxpool"
	goredis "github.com/redis/go-redis/v9"
	"log/slog"
	"sso/internal/config"
	"sso/internal/lib/sl"
	"sso/internal/storage/postgres"
	"sso/internal/storage/redis"
)

var (
	ErrUserExists    = errors.New("user already exists")
	ErrUserNotFound  = errors.New("user not found")
	ErrTokenNotMatch = errors.New("token not match")
	ErrTokenExpired  = errors.New("token expired")
)

const (
	refreshToken = "refresh_token"
	expiredAt    = "expired_at"
)

type Storage struct {
	db    *pgxpool.Pool
	cache *goredis.Client
	log   *slog.Logger
}

func New(log *slog.Logger, cfg *config.Config) (*Storage, error) {
	db, err := postgres.New(log, cfg)
	if err != nil {
		return nil, err
	}

	cache, err := redis.New(log, cfg)
	if err != nil {
		return nil, err
	}

	return &Storage{
		db:    db,
		cache: cache,
		log:   log,
	}, nil
}

func (s *Storage) Close() {
	s.db.Close()

	err := s.cache.Close()
	if err != nil {
		s.log.Error("failed to close redis connection", sl.Err(err))
		return
	}
}
