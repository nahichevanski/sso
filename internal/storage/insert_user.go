package storage

import (
	"context"
	"errors"
	"fmt"

	"github.com/jackc/pgx/v5/pgconn"
)

func (s *Storage) InsertUser(ctx context.Context, email string, passHash []byte) (int64, error) {
	const oper = "postgres.InsertUser"

	var userId int64

	query := `INSERT INTO users (email, pass_hash) VALUES ($1, $2) RETURNING id`

	err := s.db.QueryRow(ctx, query, email, passHash).Scan(&userId)
	if err != nil {
		var pgErr *pgconn.PgError
		if errors.As(err, &pgErr) && pgErr.Code == "23505" {
			return 0, ErrUserExists
		}
		return 0, fmt.Errorf("%s: %w", oper, err)
	}

	return userId, nil
}
