package storage

import (
	"context"
	"fmt"
	"sso/internal/domain/model"
	"strconv"
)

func (s *Storage) SaveSession(ctx context.Context, session model.Session) error {
	const oper = "storage.SaveSession"

	cmd := s.cache.HSet(ctx,
		fmt.Sprintf("%d", session.UserID),
		refreshToken, session.RefreshToken, expiredAt, session.ExpiresAt,
	)
	if err := cmd.Err(); err != nil {
		return fmt.Errorf("%s: %w", oper, err)
	}

	boolCmd := s.cache.Expire(ctx, strconv.FormatInt(session.UserID, 10), session.Lifetime)
	if err := boolCmd.Err(); err != nil {
		return fmt.Errorf("%s: %w", oper, err)
	}

	return nil
}
