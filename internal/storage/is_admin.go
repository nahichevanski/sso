package storage

import (
	"context"
	"errors"
	"fmt"

	"github.com/jackc/pgx/v5"
)

func (s *Storage) IsAdmin(ctx context.Context, userID int64) (bool, error) {
	const oper = "postgres.IsAdmin"

	query := `SELECT user_id FROM admins WHERE id = $1`

	adminID := 0
	err := s.db.QueryRow(ctx, query, userID).Scan(&adminID)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return false, ErrUserNotFound
		}
		return false, fmt.Errorf("%s: %w", oper, err)
	}

	return true, nil
}
