package storage

import (
	"context"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v5/pgconn"
)

func (s *Storage) DeleteUser(ctx context.Context, userID int64) (bool, error) {
	const oper = "postgres.DeleteUser"

	query := `DELETE FROM users WHERE id = $1`

	rows, err := s.db.Exec(ctx, query, userID)
	if err != nil {
		var pgErr *pgconn.PgError
		if errors.As(err, &pgErr) {
			if pgErr.Code == "00000" {
				return true, nil
			}
		}
		return false, fmt.Errorf("%s: %w", oper, err)
	}

	if rowsAffected := rows.RowsAffected(); rowsAffected == 0 {
		return false, ErrUserNotFound
	}

	return true, nil
}
