package serviceauth

import (
	"context"
	"errors"
	"fmt"
	"golang.org/x/crypto/bcrypt"
	"log/slog"
	"sso/internal/lib/sl"
	"sso/internal/storage"
)

func (a *ServiceAuth) RegisterNewUser(ctx context.Context, email, password string) (int64, error) {
	const oper = "serviceauth.RegisterNewUser"

	passHash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	if err != nil {
		a.log.Error("failed to hash password", sl.Err(err))
		return 0, fmt.Errorf("%s: %w", oper, err)
	}

	id, err := a.userDataChanger.InsertUser(ctx, email, passHash)
	if err != nil {
		if errors.Is(err, storage.ErrUserExists) {
			a.log.Warn("user already exists", sl.Err(err))
			return 0, fmt.Errorf("%s: %w", oper, ErrUserExists)
		}
		a.log.Error("failed to save user", sl.Err(err))
		return 0, fmt.Errorf("%s: %w", oper, err)
	}

	a.log.Info("new user created", slog.String("oper", oper), slog.Int64("id", id))

	return id, nil
}
