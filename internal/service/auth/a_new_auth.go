package serviceauth

import (
	"context"
	"errors"
	"log/slog"
	"sso/internal/config"
	"sso/internal/domain/model"
)

var (
	ErrInvalidCredentials = errors.New("invalid credentials")
	ErrUserExists         = errors.New("user already exists")
	ErrTokenExpired       = errors.New("refresh token expired")
	ErrTokenNotMatch      = errors.New("refresh token not match")
)

type ServiceAuth struct {
	log             *slog.Logger
	cfg             *config.Config
	userDataChanger UserDataChanger
	userDataReader  UserDataReader
	sessionManager  SessionManager
}

type UserDataChanger interface {
	InsertUser(ctx context.Context, email string, passHash []byte) (userID int64, err error)
	DeleteUser(ctx context.Context, userId int64) (isDelete bool, err error)
}

type UserDataReader interface {
	SelectUser(ctx context.Context, email string) (user model.User, err error)
	IsAdmin(ctx context.Context, userId int64) (isAdmin bool, err error)
}

type SessionManager interface {
	SaveSession(ctx context.Context, session model.Session) error
	RefreshSession(ctx context.Context, newSession model.Session, oldRefreshToken string) error
	DeleteSession(ctx context.Context, userId int64) error
}

func New(
	log *slog.Logger,
	cfg *config.Config,
	userDataChanger UserDataChanger,
	userDataReader UserDataReader,
	sessionManager SessionManager,
) *ServiceAuth {
	return &ServiceAuth{
		log:             log,
		cfg:             cfg,
		userDataChanger: userDataChanger,
		userDataReader:  userDataReader,
		sessionManager:  sessionManager,
	}
}
