package serviceauth

import (
	"context"
	"errors"
	"fmt"
	"sso/internal/domain/model"
	"sso/internal/lib/jwt"
	"sso/internal/storage"
	"time"
)

func (a *ServiceAuth) RefreshToken(ctx context.Context, userID int64, refreshToken string) (model.Token, error) {
	const oper = "service.Auth.RefreshToken"

	newSession := model.Session{
		UserID:       userID,
		RefreshToken: jwt.NewRefreshToken(),
		ExpiresAt:    time.Now().Add(a.cfg.RefreshTokenTTL),
		Lifetime:     a.cfg.SessionTTL,
	}

	err := a.sessionManager.RefreshSession(ctx, newSession, refreshToken)
	if err != nil {
		if errors.Is(err, storage.ErrTokenExpired) {
			return model.Token{}, fmt.Errorf("%s:  %w", oper, ErrTokenExpired)
		}
		if errors.Is(err, storage.ErrTokenNotMatch) {
			return model.Token{}, fmt.Errorf("%s:  %w", oper, ErrTokenNotMatch)
		}
		return model.Token{}, fmt.Errorf("%s: %w", oper, err)
	}

	accessToken, err := jwt.NewToken(model.User{ID: userID}, a.cfg.AccessTokenTTL)

	return model.Token{
		AccessToken:  accessToken,
		RefreshToken: newSession.RefreshToken,
	}, nil
}
