package serviceauth

import (
	"context"
	"errors"
	"fmt"
	"golang.org/x/crypto/bcrypt"
	"sso/internal/domain/model"
	"sso/internal/lib/jwt"
	"sso/internal/lib/sl"
	"sso/internal/storage"
	"time"
)

func (a *ServiceAuth) EnterUser(ctx context.Context, email, password string) (model.Token, error) {
	const oper = "serviceauth.EnterUser"

	user, err := a.userDataReader.SelectUser(ctx, email)
	if err != nil {
		if errors.Is(err, storage.ErrUserNotFound) {
			a.log.Warn("user not found", sl.Err(err))
			return model.Token{}, fmt.Errorf("%s: %w", oper, ErrInvalidCredentials)
		}
		a.log.Error("failed to get user", sl.Err(err))
		return model.Token{}, fmt.Errorf("%s: %w", oper, err)
	}

	err = bcrypt.CompareHashAndPassword(user.PassHash, []byte(password))
	if err != nil {
		a.log.Warn("invalid credentials", sl.Err(err))
		return model.Token{}, fmt.Errorf("%s: %w", oper, ErrInvalidCredentials)
	}

	accessToken, err := jwt.NewToken(user, a.cfg.AccessTokenTTL)
	if err != nil {
		a.log.Error("failed to create token", sl.Err(err))
		return model.Token{}, fmt.Errorf("%s: %w", oper, err)
	}

	refreshToken := jwt.NewRefreshToken()

	token := model.Token{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}

	session := model.Session{
		UserID:       user.ID,
		RefreshToken: refreshToken,
		ExpiresAt:    time.Now().Add(a.cfg.RefreshTokenTTL),
		Lifetime:     a.cfg.SessionTTL,
	}

	err = a.sessionManager.SaveSession(ctx, session)
	if err != nil {
		a.log.Error(oper, sl.Err(err))
		return model.Token{}, fmt.Errorf("%s: %w", oper, err)
	}

	return token, nil
}
