package serviceauth

import (
	"context"
	"fmt"
)

func (a *ServiceAuth) OutUser(ctx context.Context, userID int64) (bool, error) {
	const oper = "serviceauth.OutUser"

	err := a.sessionManager.DeleteSession(ctx, userID)
	if err != nil {
		return false, fmt.Errorf("%s: %w", oper, err)
	}

	return true, nil
}
