package serviceauth

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"sso/internal/lib/sl"
	"sso/internal/storage"
)

func (a *ServiceAuth) IsAdmin(ctx context.Context, userID int64) (bool, error) {
	const oper = "serviceAuth.IsAdmin"

	isAdmin, err := a.userDataReader.IsAdmin(ctx, userID)
	if err != nil {
		if errors.Is(err, storage.ErrUserNotFound) {
			a.log.Warn("user not found", sl.Err(err))
			return false, fmt.Errorf("%s:  %w", oper, ErrInvalidCredentials)
		}
		a.log.Error("failed to check admin", sl.Err(err))
		return false, fmt.Errorf("%s: %w", oper, err)
	}

	a.log.Info("user is admin", slog.Int64("userID", userID), slog.Bool("isAdmin", isAdmin))

	return isAdmin, nil
}
