package serviceauth

import (
	"context"
	"fmt"
	"log/slog"
	"sso/internal/lib/sl"
)

func (a *ServiceAuth) RemoveUser(ctx context.Context, userID int64) (bool, error) {
	const oper = "serviceauth.DeleteUser"

	isDeleted, err := a.userDataChanger.DeleteUser(ctx, userID)
	if err != nil {
		a.log.Error("failed to delete user", sl.Err(err))
		return false, fmt.Errorf("%s: %w", oper, err)
	}

	err = a.sessionManager.DeleteSession(ctx, userID)
	if err != nil {
		a.log.Error("failed to delete session", sl.Err(err))
	}

	a.log.Info("user deleted", slog.Int64("user_id", userID), slog.Bool("is_deleted", isDeleted))

	return isDeleted, nil
}
