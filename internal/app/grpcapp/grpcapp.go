package grpcapp

import (
	"context"
	"fmt"
	"log/slog"
	"net"

	serverauth "sso/internal/grpcserver/auth"

	"github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/logging"
	"github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/recovery"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type GRPCApp struct {
	log        *slog.Logger
	gRPCServer *grpc.Server
	port       int
}

func New(log *slog.Logger, port int, auth serverauth.Auth) *GRPCApp {
	loggingOpts := []logging.Option{
		logging.WithLogOnEvents(
		//logging.StartCall, logging.FinishCall,
		//logging.PayloadReceived, logging.PayloadSent,
		),
		// Add any other option (check functions starting with logging.With).
	}

	recoveryOpts := []recovery.Option{
		recovery.WithRecoveryHandler(func(p interface{}) (err error) {
			log.Error("Recovered from panic", slog.Any("panic", p))

			return status.Errorf(codes.Internal, "internal error")
		}),
	}

	gRPCServer := grpc.NewServer(grpc.ChainUnaryInterceptor(
		recovery.UnaryServerInterceptor(recoveryOpts...),
		logging.UnaryServerInterceptor(interceptorLogger(log), loggingOpts...),
	))
	serverauth.Register(gRPCServer, auth)

	return &GRPCApp{
		log:        log,
		gRPCServer: gRPCServer,
		port:       port,
	}
}

func (a *GRPCApp) Start() error {
	const oper = "grpcapp.Start"

	log := a.log.With(slog.String("oper", oper))

	l, err := net.Listen("tcp", fmt.Sprintf(":%d", a.port))
	if err != nil {
		return fmt.Errorf("%s: %w", oper, err)
	}

	log.Info("grpc server started", slog.String("addr", l.Addr().String()))

	if err = a.gRPCServer.Serve(l); err != nil {
		return fmt.Errorf("%s: %w", oper, err)
	}

	return nil
}

func (a *GRPCApp) MustStart() {
	if err := a.Start(); err != nil {
		panic(err)
	}
}

func (a *GRPCApp) Stop() {
	const oper = "grpcapp.Stop"

	a.gRPCServer.GracefulStop()

	a.log.With(slog.String("oper", oper)).
		Info("grpc server stopped", slog.Int("port", a.port))
}

// InterceptorLogger adapts slog logger to interceptor logger.
// This code is simple enough to be copied and not imported.
func interceptorLogger(l *slog.Logger) logging.Logger {
	return logging.LoggerFunc(func(ctx context.Context, lvl logging.Level, msg string, fields ...any) {
		l.Log(ctx, slog.Level(lvl), msg, fields...)
	})
}
