package app

import (
	"log/slog"
	"sso/internal/app/grpcapp"
	"sso/internal/config"
	serviceauth "sso/internal/service/auth"
	"sso/internal/storage"
)

type App struct {
	GRPCApp *grpcapp.GRPCApp
	Storage *storage.Storage
}

func New(log *slog.Logger, cfg *config.Config) *App {
	s, err := storage.New(log, cfg)
	if err != nil {
		panic(err)
	}

	service := serviceauth.New(log, cfg, s, s, s)

	grpcApp := grpcapp.New(log, cfg.GRPC.Port, service)

	return &App{
		GRPCApp: grpcApp,
		Storage: s,
	}
}
