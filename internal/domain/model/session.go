package model

import "time"

type Session struct {
	UserID       int64         `json:"user_id"`
	RefreshToken string        `json:"refresh_token"`
	ExpiresAt    time.Time     `json:"expires_at"`
	Lifetime     time.Duration `json:"lifetime"`
}
