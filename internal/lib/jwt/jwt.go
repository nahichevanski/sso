package jwt

import (
	"github.com/golang-jwt/jwt/v5"
	"math/rand/v2"
	"os"
	"sso/internal/domain/model"
	"time"
)

func NewToken(user model.User, duration time.Duration) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["uid"] = user.ID
	claims["custom_exp"] = time.Now().Add(duration).Unix()

	tokenString, err := token.SignedString([]byte(os.Getenv("MAIN_PAGE_KEY")))
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func NewRefreshToken() string {
	symbols := []byte("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	idx := len(symbols)
	token := make([]byte, 32)
	for i := 0; i < 32; i++ {
		token[i] = symbols[rand.IntN(idx)]
	}

	return string(token)
}
