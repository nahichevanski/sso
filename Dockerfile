FROM golang:1.22-alpine AS builder

WORKDIR /sso

COPY go.mod go.sum ./

RUN apk --no-cache add bash

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o ./cmd/sso/sso ./cmd/sso/main.go

FROM alpine AS runner

WORKDIR /sso

COPY --from=builder sso/cmd/sso/sso .
COPY --from=builder sso/config/prod.yaml ./config/
COPY --from=builder sso/.env .

CMD ["./sso"]

EXPOSE 24680