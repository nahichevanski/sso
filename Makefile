run:
	go run ./cmd/sso/main.go

build:
	go build ./cmd/sso/main.go

migrate-up:
	go run ./cmd/migrations/up/up.go

migrate-down:
	go run ./cmd/migrations/down/down.go

up:
	sudo docker-compose up -d

down:
	 sudo docker-compose down

stop:
	 sudo docker-compose stop

log:
	sudo docker-compose logs sso-api

rmf: ## Удалить только образы текущего проекта
rmf:
	sudo docker-compose rm -f

rmi:
	sudo docker image rm sso_sso-api && \
	sudo docker image rm sso_migrations

prune: ## Очистить только build cache
prune:
	sudo docker builder prune -f