package main

import (
	"errors"
	"fmt"
	"log/slog"
	"os"

	"sso/internal/config"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

func main() {

	cfg := config.MustLoad()

	log := slog.New(
		slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelInfo}),
	)

	conn := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
		cfg.DB.User, os.Getenv("POSTGRES_PASSWORD"), cfg.DB.Host, cfg.DB.Port, cfg.DB.Name)

	migPath := "migrations"

	m, err := migrate.New(fmt.Sprintf("file://%s", migPath), conn)
	if err != nil {
		log.Error(err.Error())
		os.Exit(1)
	}

	err = m.Down()
	if err != nil {
		if errors.Is(err, migrate.ErrNoChange) {
			log.Warn("No migrations to revert")
			return
		}
		log.Error(err.Error())
		os.Exit(1)
	}

	log.Info("Successfully reverted migrations")
}
