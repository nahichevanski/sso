FROM golang:1.22-alpine AS builder

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o migration ./cmd/migrations/up/up.go

FROM alpine AS runner

WORKDIR /app

COPY --from=builder /app/migration .
COPY --from=builder /app/config/prod.yaml ./config/
COPY --from=builder /app/.env .
COPY --from=builder /app/migrations ./migrations

CMD ["./migration"]